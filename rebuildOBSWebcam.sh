#!/bin/sh

#-------------------------------------------------
# Build installs to default directory: 
#    /usr/lib64/obs-plugins/v4l2sink.so
#-------------------------------------------------

cd /opt
git clone https://github.com/umlaeute/v4l2loopback.git
cd v4l2loopback
make
sudo make install
sudo depmod -a
sudo modprobe v4l2loopback devices=1 video_nr=10 card_label="OBS Cam" exclusive_caps=1
cd /opt
git clone --recursive https://github.com/obsproject/obs-studio.git
git clone https://github.com/CatxFish/obs-v4l2sink.git
mkdir -p obs-v4l2sink/build
cd obs-v4l2sink/build
cmake -DLIBOBS_INCLUDE_DIR="../../obs-studio/libobs" -DCMAKE_INSTALL_PREFIX=/usr ..
make -j4
sudo make install
mkdir -p ~/.config/obs-studio/plugins/v4l2sink/bin/64bit
#sudo mv /usr/lib/obs-plugins/v4l2sink.so ~/.config/obs-studio/plugins/v4l2sink/bin/64bit
rm -rf /opt/v4l2loopback
rm -rf /opt/obs-v4l2sink
rm -rf /opt/obs-studio
exit 0