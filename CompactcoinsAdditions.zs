//Scale up from a 25 coin to 50k
recipes.addShapeless("coin_scale_01",<lodsofemone:coin_small>.withTag({value: 50 as long}), [<lodsofemone:coin_small>.withTag({value: 25 as long}), <lodsofemone:coin_small>.withTag({value: 25 as long})]);
recipes.addShapeless("coin_scale_02",<lodsofemone:coin_small>.withTag({value: 50 as long}), [<lodsofemone:coin_small>.withTag({value: 10 as long}), <lodsofemone:coin_small>.withTag({value: 10 as long}),<lodsofemone:coin_small>.withTag({value: 10 as long}), <lodsofemone:coin_small>.withTag({value: 10 as long}),<lodsofemone:coin_small>.withTag({value: 10 as long})]);
recipes.addShapeless("coin_scale_03",<lodsofemone:coin_big>.withTag({value: 100 as long}), [<lodsofemone:coin_small>.withTag({value: 50 as long}), <lodsofemone:coin_small>.withTag({value: 50 as long})]);
recipes.addShapeless("coin_scale_04",<lodsofemone:coin_big>.withTag({value: 100 as long}), [<lodsofemone:coin_small>.withTag({value: 25 as long}), <lodsofemone:coin_small>.withTag({value: 25 as long}), <lodsofemone:coin_small>.withTag({value: 25 as long}), <lodsofemone:coin_small>.withTag({value: 25 as long})]);
recipes.addShapeless("coin_scale_05",<lodsofemone:coin_big>.withTag({value: 500 as long}), [<lodsofemone:coin_big>.withTag({value: 100 as long}), <lodsofemone:coin_big>.withTag({value: 100 as long}), <lodsofemone:coin_big>.withTag({value: 100 as long}), <lodsofemone:coin_big>.withTag({value: 100 as long}), <lodsofemone:coin_big>.withTag({value: 100 as long})]);
recipes.addShapeless("coin_scale_06",<lodsofemone:coin_big>.withTag({value: 1000 as long}), [<lodsofemone:coin_big>.withTag({value: 500 as long}), <lodsofemone:coin_big>.withTag({value: 500 as long})]);
recipes.addShapeless("coin_scale_07",<lodsofemone:coin_big>.withTag({value: 5000 as long}), [<lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long})]);
recipes.addShapeless("coin_scale_08",<lodsofemone:coin_big>.withTag({value: 1000 as long}), [<lodsofemone:coin_big>.withTag({value: 500 as long}), <lodsofemone:coin_big>.withTag({value: 500 as long})]);
recipes.addShapeless("coin_scale_09",<lodsofemone:coin_big>.withTag({value: 5000 as long}), [<lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long}), <lodsofemone:coin_big>.withTag({value: 1000 as long})]);
recipes.addShapeless("coin_scale_10",<lodsofemone:coin_big>.withTag({value: 10000 as long}), [<lodsofemone:coin_big>.withTag({value: 5000 as long}), <lodsofemone:coin_big>.withTag({value: 5000 as long})]);
recipes.addShapeless("coin_scale_11",<lodsofemone:coin_big>.withTag({value: 50000 as long}), [<lodsofemone:coin_big>.withTag({value: 10000 as long}), <lodsofemone:coin_big>.withTag({value: 10000 as long}), <lodsofemone:coin_big>.withTag({value: 10000 as long}), <lodsofemone:coin_big>.withTag({value: 10000 as long}), <lodsofemone:coin_big>.withTag({value: 10000 as long})]);

//Scale down from a 50k coin to 25
recipes.addShapeless("coin_scale_12",<lodsofemone:coin_big>.withTag({value: 10000 as long}) * 5, [<lodsofemone:coin_big>.withTag({value: 50000 as long})]);
recipes.addShapeless("coin_scale_13",<lodsofemone:coin_big>.withTag({value: 5000 as long}) * 2, [<lodsofemone:coin_big>.withTag({value: 10000 as long})]);
recipes.addShapeless("coin_scale_14",<lodsofemone:coin_big>.withTag({value: 1000 as long}) * 5, [<lodsofemone:coin_big>.withTag({value: 5000 as long})]);
recipes.addShapeless("coin_scale_15",<lodsofemone:coin_big>.withTag({value: 500 as long}) * 2, [<lodsofemone:coin_big>.withTag({value: 1000 as long})]);
recipes.addShapeless("coin_scale_16",<lodsofemone:coin_big>.withTag({value: 100 as long}) * 5, [<lodsofemone:coin_big>.withTag({value: 500 as long})]);
recipes.addShapeless("coin_scale_17",<lodsofemone:coin_small>.withTag({value: 50 as long}) * 2, [<lodsofemone:coin_big>.withTag({value: 100 as long})]);
recipes.addShapeless("coin_scale_18",<lodsofemone:coin_small>.withTag({value: 10 as long}) * 5, [<lodsofemone:coin_small>.withTag({value: 50 as long})]);
recipes.addShapeless("coin_scale_19",<lodsofemone:coin_small>.withTag({value: 25 as long}) * 2, [<lodsofemone:coin_small>.withTag({value: 50 as long})]);

//Add recipes to JEI
mods.jei.JEI.addItem(<lodsofemone:coin_small>.withTag({value: 50 as long}));
mods.jei.JEI.addItem(<lodsofemone:coin_big>.withTag({value: 100 as long}));
mods.jei.JEI.addItem(<lodsofemone:coin_big>.withTag({value: 500 as long}));
mods.jei.JEI.addItem(<lodsofemone:coin_big>.withTag({value: 1000 as long}));
mods.jei.JEI.addItem(<lodsofemone:coin_big>.withTag({value: 5000 as long}));
mods.jei.JEI.addItem(<lodsofemone:coin_big>.withTag({value: 10000 as long}));
mods.jei.JEI.addItem(<lodsofemone:coin_big>.withTag({value: 50000 as long}));