#InstallKeybdHook
#SingleInstance force

; -----------------------------------------
; Remap browser keys to control the media
; -----------------------------------------

Browser_Back::Send {Media_Prev}
Browser_Forward::Send {Media_Next}

; -----------------------------------------
; CTRL+Space to pin windows to top (toggle)
; -----------------------------------------

^SPACE::Winset, Alwaysontop, ,A

; -----------------------------------------
; Win+S to send the shrug emoji  ¯\_(ツ)_/¯
; -----------------------------------------

#s::Send {U+FFe3}\_({U+30c4})_/{U+FFe3}

; -----------------------------------------
; Win+B to send the ",border=1" for JIRA
; -----------------------------------------

#b::Send `,border=1

; -----------------------------------------
; Ctrl+Shift+M to send the EM Dash
; -----------------------------------------

^+m::Send —