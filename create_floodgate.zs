/*
 Requires the Buildcraft jar to be installed in the mods folder.
 This script simply removes all recipes except the flood gate, 
 then modifies its recipe using a new fluid tank oredict
*/

import crafttweaker.item.IItemStack;
import crafttweaker.oredict.IOreDict;
import crafttweaker.oredict.IOreDictEntry;
import crafttweaker.mods.ILoadedMods;
import crafttweaker.mods.IMod;

#packmode normal

// Create array of all Buildcraft modids

	var buildcraftMods = ["buildcraftcore","buildcraftbuilders","buildcrafttransport","buildcraftsilicon","buildcraftcompat","buildcraftenergy","buildcraftfactory","buildcraftrobotics"] as string[];

// Loop through all the items in all those mods and remove their recipes. Also remove and hide the JEI entry, except for the flood gate (since it happens much later)

	print(" ==== Cleaning up the Buildcraft recipes");

	for mod in buildcraftMods {
		for item in loadedMods[mod].items {
			//print("   === Removing recipe and from JEI: " ~ item.name ~ " (" ~ item.displayName ~ ")");
			recipes.remove(item);
			if (item.name != "tile.floodGateBlock") {
				mods.jei.JEI.removeAndHide(item);
			}
		}
	}

// Create oredict for fluid tanks. If any of these mods are removed from ATM, this will fail

	print(" ==== Creating the fluidTank array");

	var fluidTank = [
		<cyclicmagic:block_storeempty>,
		<enderio:block_tank:0>,
		<enderio:block_tank:1>,
		<advancedrocketry:liquidtank>,
		<embers:block_tank>,
		<enderstorage:ender_storage:1>,
		<extracells:certustank>,
		<forestry:raintank>,
		<industrialforegoing:black_hole_tank>,
		<ic2:te:81>,
		<ic2:te:131>,
		<ic2:te:132>,
		<ic2:te:133>,
		<ic2:te:134>,
		<mekanism:basicblock:9>,
		<mekanism:machineblock2:11>,
		<openblocks:tank>,
		<overloaded:infinite_tank>,
		<quantumstorage:quantum_tank>,
		<thermalexpansion:tank>
	] as IItemStack[];
	
	print(" ==== Creating the fluidTank oreDict entry");
	<ore:fluidTank>.addItems(fluidTank);

// Create recipe for flood gate using the new fluidTank ore dictionary

	print(" ==== Creating the flood gate recipe");
	
	recipes.addShaped(<buildcraftfactory:flood_gate>, [
		[<ore:ingotIron>, <ore:gearIron>, <ore:ingotIron>],
		[<minecraft:iron_bars>,<ore:fluidTank>,<minecraft:iron_bars>],
		[<ore:ingotIron>,<minecraft:iron_bars>,<ore:ingotIron>]
	]);	  