# Utilities I made/use:

* [**Outlook Meeting De-Reminder**](https://gitlab.com/chirptheboy/utils/blob/master/dereminder.vba) - A vba script that will remove the reminder from all-day meeting requests. I'm not sure this fully works.
* [**Rebuild v4l2sink for OBS**](https://gitlab.com/chirptheboy/utils/blob/master/rebuildOBSWebcam.sh) - A shell script that rebuilds the OBS plugin v4l2sink, which lets you 'stream' your OBS to a virtual webcam.
* **Spotify Now Playing** - A bash script that gets the title and artist of the currently-playing track with mpris
* [**AHK Hotkeys**](https://gitlab.com/chirptheboy/utils/blob/master/hotkeys.ahk) - A collection of hotkeys for AutoHotkey, including toggling Always On Top for the current window, remapping the browser keys for media playback, and the shrug emoji.
* **FFMPEG Scripts** - Record video from VHS via composite adapter; create a video from an mp3 and an image; extract a portion of a video into a new file
* **CraftTweaker Scripts** - [Create flood gate recipe from BuildCraft mod](https://gitlab.com/chirptheboy/utils/blob/master/create_floodgate.zs)