Option Explicit

Private WithEvents Items As Outlook.Items


Private Sub Application_Startup()

  Dim olApp As Outlook.Application
  Dim olNS As Outlook.NameSpace
  Set olApp = Outlook.Application
  Set olNS = olApp.GetNamespace("MAPI")
  Set Items = olNS.GetDefaultFolder(olFolderInbox).Items
  
End Sub


Private Sub Items_ItemAdd(ByVal Item As Object)

  On Error Resume Next
  Dim Meet As Outlook.MeetingItem
  Dim Appt As Outlook.AppointmentItem

    If TypeOf Item Is Outlook.MeetingItem Then
    Set Meet = Item

    'Meet.ReminderSet = False
    'Meet.Save

    Set Appt = Meet.GetAssociatedAppointment(True)

    If Not Appt Is Nothing And Appt.AllDayEvent And Appt.MeetingStatus <> 5 Then
        
        Appt.ReminderSet = False
        Appt.Save
        
    End If
    
  End If
  
End Sub